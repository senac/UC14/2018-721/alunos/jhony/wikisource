
package br.com.senac.wikisource.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jhony
 */
@WebServlet(name = "ProcessarFormulario", urlPatterns = {"/paginas/ProcessarFormulario"})
public class ProcessarFormulario extends HttpServlet {

 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)       throws ServletException, IOException {
        
        boolean isInformacaoValida = false;
        boolean isReceberInformacoes = false;
        
        String nome = request.getParameter("nome");
        String assunto = request.getParameter("categorias");
        String mensagem = request.getParameter("mensagem");
        String informacoesValidas = request.getParameter("info");
        isInformacaoValida = informacoesValidas.equals("sim");
        String receberInformacoes = request.getParameter("informacoes");
        isReceberInformacoes = receberInformacoes != null;
        
        System.out.println("Nome:" + nome);
        System.out.println("Assunto:" + assunto);
        System.out.println("Mensagem:" + mensagem);
        System.out.println("Informacoes Validas:" + isInformacaoValida);
        System.out.println("ReceberInformacoes:" + isReceberInformacoes);
     
        String categoria = null;
        if (categoria.equalsIgnoreCase("adicao")) {
            categoria = "Adição de conteúdo";
        } else if (categoria.equalsIgnoreCase("remocao")) {
            categoria = "Remoção de conteúdo";
        } else if (categoria.equalsIgnoreCase("correcao")) {
            categoria = "Correção de conteúdo";
        }
        
        String infoValida = null;
        if (isInformacaoValida) {
            infoValida = "Sim";
        } else {
            infoValida = "Não";
        }
        
        String receberInformacoesV = null;
        if (isReceberInformacoes) {
            receberInformacoes = "Sim";
        } else {
            receberInformacoes = "Não";
        }
        
        
        
        
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
